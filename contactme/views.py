from django.shortcuts import render, redirect
from .forms import FormMessage
from .models import Message

# Create your views here.

def create_message(request):
    form = FormMessage(request.POST or None)
    response = {}
    if(request.method == "POST" and form.is_valid()):
        name = request.POST.get("name")
        message = request.POST.get("message")
        Message.objects.create(name=name, message=message)
        return redirect('contact')

    messages = Message.objects.all().order_by("-id")
    response = {
        "messages" : messages,
        "form" : form,
    }
    return render(request, 'contactme.html', response)