from django import forms

class FormMessage(forms.Form):
    name = forms.CharField(label="Name", required=True,)
    message = forms.CharField(label="Message", required=True, widget=forms.Textarea)