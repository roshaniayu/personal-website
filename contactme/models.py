from django.db import models
from django.utils import timezone

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=50)
    message = models.CharField(max_length=2048)
    created_at = models.DateTimeField(auto_now_add=True)