from django.shortcuts import render

# Create your views here.
def experiences(request):
    response = {}
    return render(request,'experiences.html',response)