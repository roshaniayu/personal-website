## Heroku Link
http://roshaniayu.herokuapp.com/

Cara untuk menjalankan project ini secara lokal:
#### **Step 1: Konfigurasi virtualenv**

1. Install virtualenv

   ```bash
   pip install virtualenv
   ```
   atau pake cara kedua kalau cara diatas gabisa.

   ```bash
   python/py (pilih salah satu) -m pip install virtualenv
   ```

   **Untuk Linux:** install virtualenv dengan cara

   ```bash
   sudo pip3 install virtualenv
   ```

2. Bikin virtualenv nya (dalam contoh ini nama folder nya 'venv')

   ```bash
   virtualenv venv
   ```
   atau pake cara kedua kalau cara diatas gabisa.

   ```bash
   python/py (pilih salah satu) -m venv venv
   ```

3. Aktifin virtualenv nya

   ```bash
   \venv\Scripts\activate.bat
   ```
   **Untuk Linux:**

   ```bash
   source venv/bin/activate
   ```

4. Masuk ke folder project, ketik

   ```bash
   pip install -r requirements.txt
   ```
   atau

   ```bash
   python/py (pilih salah satu) -m pip install -r requirements.txt
   ```

5. Tunggu instalasi selesai

#### **Step 2: Runserver pada local**
1. Menjalankan

   ```bash
   python manage.py runserver 8000
   ```

2. Buka browser favorit anda, ketikkan alamat

   ```bash
   http://localhost:8000
   ```

3. Selesaiiiiii :)

Asumsi saya content website yang saya buat sudah memenuhi kriteria yang diminta. Ide dalam pengembangan website ini terinspirasi dari LinkedIn. Penggunaan warna cerah pada website untuk menyampaikan suasana positif bagi pengunjung website. Website yang responsif bertujuan untuk mempermudah akses melalui perangkat Desktop maupun Mobile.