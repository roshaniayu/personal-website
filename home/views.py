from django.shortcuts import render

# Create your views here.
def home(request):
    response = {}
    return render(request,'home.html',response)