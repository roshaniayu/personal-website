from django.shortcuts import render

# Create your views here.
def profile(request):
    response = {}
    return render(request,'profile.html',response)
